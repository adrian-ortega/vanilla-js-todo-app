class EventBus {
  constructor () {
    this.events = []
  }

  /**
   * @param eventType
   * @returns {EventBusEvent|boolean}
   */
  getEvent(eventType) {
    return this.events.find(pair => pair.eventType === eventType)
  }

  addEvent(eventType, callback) {
    this.events.push(new EventBusEvent(eventType, callback))
  }

  /**
   * @param {string|Array<string>} eventType
   * @param callback
   */
  on (eventType, callback) {
    if(Array.isArray(eventType)) {
      eventType.map((evt) => this.on(evt, callback))
      return
    }

    const event = this.getEvent(eventType)

    if(event) {
      event.subscribe(callback)
    } else {
      this.addEvent(eventType, callback)
    }
  }

  /**
   * @param {string} eventType
   * @param {*} args
   */
  emit (eventType, args = null) {
    const eventCallbacksPair = this.getEvent(eventType)
    if(eventCallbacksPair) {
      eventCallbacksPair.dispatch(args)
    }
  }
}

window.AppBus = new EventBus()
