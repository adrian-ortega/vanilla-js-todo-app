class EventBusEvent {
  /**
   * @param {string} eventType
   * @param {function} callback
   */
  constructor (eventType, callback) {
    this.eventType = eventType;
    this.callbacks = [callback];
  }

  /**
   * @param {function} callback
   */
  subscribe(callback = () => {}) {
    this.callbacks.push(callback)
  }

  /**
   * @param {*} args
   */
  dispatch (args = null) {
    this.callbacks.forEach(callback => callback(args))
  }
}
