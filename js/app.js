class App {
  constructor ({mount, components}) {
    this.$el = AppUtil.el(mount)
    this.components = components
    this.init()
  }

  init () {
    Object.entries(this.components).forEach(([componentName, componentClass]) => {
      const componentInstance = new componentClass({}, {
        mount: this.$el
      })

      componentInstance.render()

      this.components[componentName] = componentInstance
    })
  }
}


window.App = new App({
  mount: '#app',
  components: {
    AppHeader,
    AppForm,
    AppTodos
  }
})
