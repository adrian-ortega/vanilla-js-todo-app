const AppUtil = {}

/**
 * Returns a single HTML element or an array of HTML elements
 * @param {string} selector
 * @param {Element|Document} context
 * @returns {*}
 */
AppUtil.el = (selector, context) => {
  if(!context) {
    context = window.document
  }

  const $el = context.querySelectorAll(selector)
  return $el.length === 0 ? null : $el.length === 1 ? $el[0] : Array.from($el)
}

/**
 * Creates an element out of a string template
 * @param {string} template
 * @returns {ChildNode}
 */
AppUtil.renderTemplate = (template) => {
  const tempDiv = document.createElement('div')
  tempDiv.innerHTML = template
  return tempDiv.firstChild
}
