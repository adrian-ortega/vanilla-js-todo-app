class AppHeader extends AppComponent {
  template () {
    return `
    <header id="header">
      <div class="container">
        <div class="columns is-centered">
          <div class="column is-6">
            <h2 class="subtitle is-size-5">Vanilla JS</h2>
            <h1 class="title is-size-1 has-text-weight-bold">Todos</h1>
          </div>
        </div>
      </div>
    </header>
    `
  }
}
