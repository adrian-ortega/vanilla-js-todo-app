class AppTodos extends AppComponent {
  setup () {
    this.storage = new LocalStorageItemList('app-todos')
    this.todos = this.storage.get()
    let id = 0

    this.$on('create-todo', (todo) => {
      this.todos.push({
        id: id++,
        value: todo,
        created: new Date(),
        edit: false,
        completed: false
      })
    })

    this.$on('update-todo', (todo) => {
      const index = this.todos.map(t => t.id).indexOf(todo.id)
      this.todos[index] = todo
      this.storage.update(this.todos)
    })

    this.$on('delete-todo', (id) => {
      this.todos = this.todos.filter(todo => todo.id !== id)
    })

    this.$on('complete-todo', (id) => {
      const index = this.todos.map(t => t.id).indexOf(id)
      this.todos[index].completed = true
    })

    this.$on(['create-todo', 'update-todo', 'complete-todo', 'delete-todo'], () => {
      this.storage.update(this.todos)
      this.rerender()
    })
  }

  mounted () {
    const items = this.el('.items')
    items.innerHTML = '<p>🤷🏻‍♂️Nothing to see here</p>'

    if(this.todos.length > 0) {
      items.innerHTML = ''
      const renderTodoComponents = components => components.map(todo => new AppTodo({todo}, {
        mount: items,
      })).map(component => component.render())

      const pendingTodos = this.todos.filter(todo => !todo.completed)
      const completedTodos = this.todos.filter(todo => todo.completed)

      renderTodoComponents(pendingTodos)

      if(completedTodos.length) {
        items.appendChild(AppUtil.renderTemplate('<h4 class="title is-size-5">Completed</h4>'))
        renderTodoComponents(completedTodos)
      }
    }
  }

  template () {
    return `
    <div class="todos">
      <div class="container">
        <div class="columns is-centered">
          <div class="column is-6">
            <div class="items">
              <!-- will get filled in after mounted -->
            </div>
          </div>
        </div>
      </div>
    </div>`
  }
}
