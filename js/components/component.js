class AppComponent {
  constructor (props = {}, options = {}) {
    this.props = {...props}
    this.$parent = options && options.mount ? options.mount : null
    this.$el = null
    this.bus = window.AppBus

    this.setup()
  }

  el (selector) {
    return AppUtil.el(selector, this.$el)
  }

  setup() {
    //
  }

  mounted() {
    //
  }

  template () {
    return ''
  }

  renderTemplate () {
    return AppUtil.renderTemplate(this.template().trim())
  }

  render () {
    this.$el = this.renderTemplate()

    if(this.$parent) {
      this.$parent.appendChild(this.$el)
    }

    this.mounted()

    return this.$el
  }

  rerender () {
    const template = this.renderTemplate()
    this.$el.innerHTML = ''
    this.$el.appendChild(...template.children)
    this.mounted()
  }

  $on (eventType, callback) {
    this.bus.on(eventType, callback)
  }

  $emit (eventType, args ) {
    this.bus.emit(eventType, args)
  }
}
