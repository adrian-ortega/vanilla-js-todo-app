class AppForm extends AppComponent {
  setup() {
    this.value = ''
  }

  mounted () {
    const $input = this.el('input')

    $input.addEventListener('input', (event) => {
      this.value = event.target.value
    })

    this.$el.addEventListener('submit', (event) => {
      event.preventDefault()
      this.$emit('create-todo', this.value)
      this.value = $input.value = ''
    })
  }

  template () {
    return `
    <form action="">
      <div class="container">
        <div class="columns is-centered">
          <div class="column is-6">
            <div class="field has-addons">
              <div class="control is-expanded">
                <input class="input is-medium" type="text" placeholder="What's next?">
              </div>
              <div class="control">
                <button type="submit" class="button is-info is-medium">Add</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>`
  }
}
