class AppTodo extends AppComponent {
  mounted () {
    const editButton = this.el('button.is-edit')

    if(editButton) {
      editButton.addEventListener('click', (e) => {
        e.preventDefault()
        this.props.todo.edit = !this.props.todo.edit

        if(this.props.todo.edit) {
          this.$emit('update-todo', {...this.props.todo})
        }

        this.rerender()
      })

      const editInput = this.el('input')
      if(editInput) {
        editInput.addEventListener('input', (event) => {
          this.props.todo.value = event.target.value
        })

        const vlen = this.props.todo.value.length
        editInput.focus()
        editInput.setSelectionRange(vlen, vlen);
      }
    }

    const deleteButton = this.el('button.is-delete')

    if(deleteButton) {
      deleteButton.addEventListener('click', (e) => {
        e.preventDefault()
        this.$emit('delete-todo', this.props.todo.id)
      })
    }

    const completeButton = this.el('button.is-complete')

    if(completeButton) {
      completeButton.addEventListener('click', (e) => {
        e.preventDefault()
        this.$emit('complete-todo', this.props.todo.id)
      })
    }
  }

  template () {
    const {todo} = this.props
    const editButton = !todo.completed
      ? `<button class="button is-edit has-icon" title="Edit">
            <span class="icon">${todo.edit ? `👍🏻` : `✏️`}</span>
         </button>`
      : ''

    const completeButton = !todo.completed && !todo.edit
      ? `<button class="button is-complete has-icon"><span class="icon">✔️</span></button>`
      : ''

    const deleteButton = !todo.edit
      ? `<button class="button is-delete has-icon"><span class="icon">🗑️</span></button>`
      : ''

    return `
    <div class="card">
      <div class="card-content">
        <div class="media">
          <div class="media-content">
            ${
              todo.edit
                ? `<input class="input" type="text" value="${todo.value}">` 
                : `<p>${todo.value}</p>`
            }
          </div>
          <div class="media-right">
            ${completeButton}
            ${editButton}
            ${deleteButton}
          </div>
        </div>  
      </div>
    </div>`
  }
}
