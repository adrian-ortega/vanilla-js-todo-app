class LocalStorageItemList {
  constructor (name, defaultData = []) {
    this.name = name
    this.data = localStorage.getItem(this.name)
      ? JSON.parse(localStorage.getItem(this.name))
      : defaultData
  }

  get () {
    return this.data
  }

  update (data) {
    this.data = data
    localStorage.setItem(this.name, JSON.stringify(this.data))
  }
}
